package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/http"
	"sync"
	"time"
)

const timeout = 5 * time.Second
const URL = "http://localhost:55555/proxy/get/binance"
const count = 50000
const poolSize = 3

func main() {
	dialer := &net.Dialer{
		Timeout:   timeout,
		KeepAlive: timeout,
	}

	httpClient := &http.Client{
		Timeout: timeout,
		Transport: &http.Transport{
			DisableKeepAlives: true,
			Dial:              dialer.Dial,
			TLSClientConfig:   &tls.Config{InsecureSkipVerify: true},
		},
	}

	request, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		log.Panic(err)
		return
	}
	request.Header.Set("Content-Type", "application/json")

	workerCh := make(chan struct{}, poolSize)

	wg := sync.WaitGroup{}
	wg.Add(poolSize)

	start := time.Now()

	n := 0
	for i := 0; i < poolSize; i++ {
		go func() {
			defer wg.Done()

			for range workerCh {
				n++
				resp, err := httpClient.Do(request)
				if err != nil {
					fmt.Println(n, err)
					continue
				}
				resp.Body.Close()
			}
		}()
	}

	for i := 0; i < count; i++ {
		workerCh <- struct{}{}
	}

	close(workerCh)

	wg.Wait()

	fmt.Println("End at: ", time.Since(start))
}
