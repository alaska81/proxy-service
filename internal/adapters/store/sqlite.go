package store

import (
	"context"
	"fmt"
	"strings"

	"proxy-service/internal/app/entity"
	"proxy-service/internal/pkg/logger"

	"github.com/jmoiron/sqlx"
)

type StoreSQLite struct {
	db  *sqlx.DB
	log *logger.Logger
}

func (s *StoreSQLite) List(ctx context.Context) (proxyList []entity.Proxy, err error) {
	query := `
		SELECT * FROM proxy_list
	`

	rows, err := s.db.QueryxContext(ctx, query)
	if err != nil {
		s.log.Warningf("List [QueryxContext]: %v", err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var proxy entity.Proxy
		err = rows.StructScan(&proxy)
		if err != nil {
			s.log.Warningf("List [StructScan]: %v", err)
			return
		}

		split := strings.Split(proxy.Proxy, ":")
		if len(split) < 3 {
			s.log.Warningf("List [broken proxy]: %s", proxy.Proxy)
			return
		}

		proxy.Port = split[2]

		proxyList = append(proxyList, proxy)
	}

	return
}

func (s *StoreSQLite) Append(ctx context.Context, proxyList []entity.Proxy) (err error) {
	query := `
		INSERT INTO proxy_list (proxy, created_at) VALUES ($1, $2)
	`
	tx, err := s.db.Begin()
	if err != nil {
		s.log.Warningf("Append [Begin]: %v", err)
		return
	}

	defer tx.Rollback()

	for _, proxy := range proxyList {
		_, err = tx.ExecContext(ctx, query, proxy.Proxy, proxy.CreatedAt)
		if err != nil {
			s.log.Warningf("Append [ExecContext]: %v", err)
			return fmt.Errorf("error append [%s]: %v", proxy.Proxy, err)
		}
	}

	err = tx.Commit()
	if err != nil {
		s.log.Warningf("Append [Commit]: %v", err)
		return
	}

	return
}

func (s *StoreSQLite) Remove(ctx context.Context, proxyList []entity.Proxy) (err error) {
	query := `
		DELETE FROM proxy_list WHERE proxy LIKE ('%' || $1 || '%')
	`
	tx, err := s.db.Begin()
	if err != nil {
		s.log.Warningf("Remove [Begin]: %v", err)
		return
	}

	defer tx.Rollback()

	for _, proxy := range proxyList {
		_, err = tx.ExecContext(ctx, query, proxy.Proxy)
		if err != nil {
			s.log.Warningf("Remove [ExecContext]: %v", err)
			return
		}
	}

	err = tx.Commit()
	if err != nil {
		s.log.Warningf("Remove [Commit]: %v", err)
		return
	}

	return
}

func NewStoreSQLite(db *sqlx.DB, log *logger.Logger) *StoreSQLite {
	query := `
		CREATE TABLE IF NOT EXISTS proxy_list (
			proxy TEXT NOT NULL PRIMARY KEY,
			created_at TEXT
		);
	`
	db.MustExec(query)

	return &StoreSQLite{
		db,
		log,
	}
}
