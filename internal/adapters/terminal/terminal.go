package terminal

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

type Terminal struct {
	URL string
}

func (t *Terminal) SendIPs(list []string) error {
	if len(list) == 0 {
		return errors.New("list is empty")
	}

	url := fmt.Sprintf("%s/terminal/ip/list", t.URL)

	client := &http.Client{
		Timeout: 10 * time.Second,
	}

	b, err := json.Marshal(struct {
		List []string `json:"list"`
	}{List: list})
	if err != nil {
		return err
	}

	resp, err := client.Post(url, "application/json", bytes.NewBuffer(b))
	if err != nil {
		return fmt.Errorf("error request: %v", err)
	}
	defer resp.Body.Close()

	var value struct {
		Success bool        `json:"success"`
		Return  interface{} `json:"return"`
		Error   string      `json:"error"`
	}
	err = json.NewDecoder(resp.Body).Decode(&value)
	if err != nil {
		return fmt.Errorf("error response: %v", err)
	}

	if !value.Success {
		return errors.New(value.Error)
	}

	return nil
}

func NewClient(URL string) *Terminal {
	if URL == "" {
		return nil
	}

	return &Terminal{
		URL,
	}
}
