package api

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	requestCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Subsystem: "proxy_service",
			Name:      "request_counter",
			Help:      "Request counter",
		},
		[]string{"exchange"},
	)

	regularGauge = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Subsystem: "proxy_service",
			Name:      "regular_gauge",
			Help:      "Regular gauge",
		},
	)

	brokenGauge = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Subsystem: "proxy_service",
			Name:      "broken_gauge",
			Help:      "Broken gauge",
		},
	)
)

func metricsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		if exchange, ok := vars["exchange"]; ok {
			exchange = strings.ToLower(exchange)
			requestCounter.WithLabelValues(exchange).Inc()
		}

		next.ServeHTTP(w, r)
	})
}

func (wr webRouter) metricsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		regularList := wr.service.GetRegularList(false)
		brokenList := wr.service.GetBrokenList(false)

		regularGauge.Set(float64(len(regularList)))
		brokenGauge.Set(float64(len(brokenList)))

		next.ServeHTTP(w, r)
	})
}

func init() {
	prometheus.MustRegister(requestCounter, regularGauge, brokenGauge)
}
