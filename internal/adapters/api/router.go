package api

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"proxy-service/internal/adapters/terminal"
	"proxy-service/internal/app/entity"
	"proxy-service/internal/pkg/logger"
)

type ProxyService interface {
	AppendList(ctx context.Context, list []entity.Proxy) error
	RemoveList(ctx context.Context, list []entity.Proxy) error
	GetRegularList(sorting bool) (list []entity.Proxy)
	GetBrokenList(sorting bool) (list []entity.Proxy)
	GetExchangeProxy(ctx context.Context, exchange string) (proxy entity.Proxy, err error)
}

type webRouter struct {
	*mux.Router
	*template.Template
	service  ProxyService
	password string
	log      *logger.Logger
	terminal *terminal.Terminal
}

var resp struct {
	Success bool   `json:"success"`
	Return  string `json:"return"`
}

const timeout = 3 * time.Second

const sessionName = "session-auth"

var sessionStore *sessions.CookieStore
var lastProxy entity.Proxy

func (wr webRouter) indexHandler(w http.ResponseWriter, r *http.Request) {
	var resp struct {
		Error       string
		List        []entity.Proxy
		RegularList []entity.Proxy
		BrokenList  []entity.Proxy
		Terminal    bool
	}

	defer func() {
		resp.RegularList = wr.service.GetRegularList(true)
		resp.BrokenList = wr.service.GetBrokenList(true)
		if wr.terminal != nil {
			resp.Terminal = true
		}

		if err := wr.ExecuteTemplate(w, "index", resp); err != nil {
			wr.log.Warnf("ExecuteTemplate: %v", err)
			w.Write([]byte(err.Error()))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}()

	if r.Method == "POST" {
		action := r.PostFormValue("action")
		list := r.PostFormValue("list")

		var proxyList []entity.Proxy
		sss := strings.Split(list, "\n")
		for _, s := range sss {
			s = strings.TrimSpace(s)
			if len(s) > 0 {
				proxyList = append(proxyList, entity.Proxy{
					Proxy:     s,
					CreatedAt: time.Now().UTC().Format("2006-01-02 15:04:05 -07"),
				})
			}
		}

		if len(proxyList) == 0 {
			resp.Error = "List is empty"
			return
		}

		ctx := context.Background()
		ctx, cancel := context.WithTimeout(ctx, timeout)
		defer cancel()

		if action == "add" {
			if err := wr.service.AppendList(ctx, proxyList); err != nil {
				resp.Error = err.Error()
				resp.List = proxyList
				return
			}
		}

		if action == "del" {
			if err := wr.service.RemoveList(ctx, proxyList); err != nil {
				resp.Error = err.Error()
				resp.List = proxyList
				return
			}
		}
	}
}

func (wr webRouter) faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "templates/favicon.ico")
}

func (wr webRouter) loginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		password := r.PostFormValue("password")
		if wr.password == password {
			session, err := sessionStore.New(r, sessionName)
			if err != nil {
				wr.log.Warnf("loginHandler New session error: %v", err)
				// 	fmt.Println(session, err)
				// 	http.Redirect(w, r, "/login", http.StatusSeeOther)
				// 	return
			}

			session.Values["auth"] = true

			err = session.Save(r, w)
			if err != nil {
				wr.log.Warnf("session.Save: %v", err)
				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}

			wr.log.Infof("loginHandler save session: %s", session.Name())

			http.Redirect(w, r, "/", http.StatusMovedPermanently)
			return
		}
		wr.log.Warnf("loginHandler: %s", "Not auth")
	}

	err := wr.ExecuteTemplate(w, "login", nil)
	if err != nil {
		wr.log.Warnf("ExecuteTemplate: %v", err)
		w.Write([]byte(err.Error()))
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (wr webRouter) proxyHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	exchange, ok := vars["exchange"]
	if !ok {
		w.Write([]byte("Parameter 'exchange' is empty"))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	proxy, err := wr.service.GetExchangeProxy(ctx, exchange)
	if err != nil {
		if lastProxy.Proxy != "" {
			wr.log.Warnf("GetExchangeProxy (%s): %v", exchange, err)

			resp.Success = true
			resp.Return = lastProxy.Proxy

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(resp)
			return
		}

		wr.log.Errorf("GetExchangeProxy (%s): %v", exchange, err)
		w.Write([]byte(err.Error()))
		w.WriteHeader(http.StatusGatewayTimeout)
		return
	}

	lastProxy = proxy

	resp.Success = true
	resp.Return = proxy.Proxy

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
	// w.Write([]byte(proxy.Proxy))
}

func (wr webRouter) terminalHandler(w http.ResponseWriter, r *http.Request) {
	list := wr.service.GetRegularList(true)
	result := make([]string, 0, len(list))

	var regex = regexp.MustCompile("[@,:]")

	for _, proxy := range list {
		split := regex.Split(proxy.Proxy, -1)
		if len(split) > 3 {
			result = append(result, split[2])
		}
	}

	var resp struct {
		Success bool   `json:"success"`
		Error   string `json:"error"`
	}

	resp.Success = true

	if err := wr.terminal.SendIPs(result); err != nil {
		resp.Success = false
		resp.Error = err.Error()
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}

func NewRouter(service ProxyService, sessionKey, templates, password string, log *logger.Logger, terminal *terminal.Terminal) (*webRouter, error) {
	sessionStore = sessions.NewCookieStore([]byte(sessionKey))
	sessionStore.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   3600 * 24, // 24 hours
		HttpOnly: true,
	}

	tmpl, err := template.ParseFiles(fmt.Sprintf("%s/index.tmpl", templates), fmt.Sprintf("%s/login.tmpl", templates))
	if err != nil {
		return nil, err
	}

	wr := &webRouter{
		mux.NewRouter(),
		tmpl,
		service,
		password,
		log,
		terminal,
	}

	wr.HandleFunc("/favicon.ico", wr.faviconHandler)
	wr.HandleFunc("/login", wr.loginHandler)
	wr.Handle("/", wr.authMiddleware(http.HandlerFunc(wr.indexHandler)))
	wr.Handle("/proxy/get/{exchange}", metricsMiddleware(http.HandlerFunc(wr.proxyHandler)))
	wr.Handle("/metrics", wr.metricsMiddleware(promhttp.Handler()))
	wr.HandleFunc("/terminal/ip", wr.terminalHandler)

	return wr, nil
}
