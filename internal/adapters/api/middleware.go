package api

import "net/http"

func (wr webRouter) authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := sessionStore.Get(r, sessionName)
		if err != nil {
			wr.log.Warnf("authMiddleware Get session error: %v", err)
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}

		auth, ok := session.Values["auth"].(bool)
		if !ok || !auth {
			wr.log.Warnf("authMiddleware: %s (%v/%v), %+v", "Not auth", auth, ok, session.Values)
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}

		next.ServeHTTP(w, r)
	})
}
