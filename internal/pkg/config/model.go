package config

import "time"

type Config struct {
	WebServer   WebServer
	Sqlite      Sqlite
	Logger      Logger
	Service     Service
	Terminal    Terminal
	Password    string
	SizeChannel int
}

type WebServer struct {
	Port      string
	Timeout   int
	Templates string
}

type Sqlite struct {
	Path         string
	File         string
	MaxOpenConns int
	MaxIdleConns int
}

type Logger struct {
	Level string
	Path  string
	File  string
}

type Service struct {
	ValidateTimeout time.Duration
	DealTimeout     time.Duration
}

type Terminal struct {
	URL string
}
