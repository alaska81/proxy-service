package config

import (
	"fmt"
	"os"
	"time"

	"gopkg.in/yaml.v3"
)

type configFile struct {
	WebServer struct {
		Port      string `yaml:"Port"`
		Timeout   int    `yaml:"Timeout"`
		Templates string `yaml:"Templates"`
	} `yaml:"webserver"`
	Sqlite struct {
		Path         string `yaml:"path"`
		File         string `yaml:"file"`
		MaxOpenConns int    `yaml:"maxOpenConns"`
		MaxIdleConns int    `yaml:"maxIdleConns"`
	} `yaml:"sqlite"`
	Logger struct {
		Level string `yaml:"level"`
		Path  string `yaml:"path"`
		File  string `yaml:"file"`
	} `yaml:"logger"`
	Service struct {
		ValidateTimeout int `yaml:"validateTimeout"`
		DealTimeout     int `yaml:"dealTimeout"`
	} `yaml:"service"`
	Terminal struct {
		URL string `yaml:"url"`
	} `yaml:"terminal"`
	Password    string `yaml:"password"`
	SizeChannel int    `yaml:"sizeChannel"`
}

func read(fileBytes []byte) (*Config, error) {
	cf := configFile{}

	err := yaml.Unmarshal(fileBytes, &cf)
	if err != nil {
		return nil, err
	}

	cfg := Config{}

	cfg.WebServer = WebServer{
		Port:      cf.WebServer.Port,
		Timeout:   cf.WebServer.Timeout,
		Templates: cf.WebServer.Templates,
	}

	cfg.Sqlite = Sqlite{
		Path:         cf.Sqlite.Path,
		File:         cf.Sqlite.File,
		MaxOpenConns: cf.Sqlite.MaxOpenConns,
		MaxIdleConns: cf.Sqlite.MaxIdleConns,
	}

	cfg.Logger = Logger{
		Level: cf.Logger.Level,
		Path:  cf.Logger.Path,
		File:  cf.Logger.File,
	}

	cfg.Service = Service{
		ValidateTimeout: time.Duration(cf.Service.ValidateTimeout) * time.Second,
		DealTimeout:     time.Duration(cf.Service.DealTimeout) * time.Second,
	}

	cfg.Terminal = Terminal{
		URL: cf.Terminal.URL,
	}

	cfg.Password = cf.Password
	cfg.SizeChannel = cf.SizeChannel

	return &cfg, nil
}

var cfg *Config

func GetFromFile(filename string) (*Config, error) {
	b, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("error open config file: %s", err.Error())
	}

	cfg, err = read(b)
	if err != nil {
		return nil, fmt.Errorf("error read config file: %s", err.Error())
	}

	return cfg, nil
}
