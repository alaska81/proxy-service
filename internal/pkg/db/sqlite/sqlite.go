package sqlite

import (
	"fmt"
	"os"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

type Config struct {
	Path         string
	Filename     string
	MaxOpenConns int
	MaxIdleConns int
}

func NewClient(cfg Config) (*sqlx.DB, error) {
	err := os.MkdirAll(cfg.Path, 0664)
	if err != nil {
		return nil, err
	}

	dsn := fmt.Sprintf("%s/%s", cfg.Path, cfg.Filename)

	db, err := sqlx.Connect("sqlite3", dsn)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(cfg.MaxOpenConns)
	db.SetMaxIdleConns(cfg.MaxIdleConns)
	db.SetConnMaxLifetime(time.Minute)

	return db, nil
}
