package webserver

import (
	"io"
	"log"
	"net/http"
	"time"

	"github.com/google/uuid"
)

type Config struct {
	Port       string
	Timeout    time.Duration
	SessionKey string
	Templates  string
}

type WebServer struct {
	*http.Server
	SessionKey string
	Templates  string
}

func (s *WebServer) Start() error {
	return s.ListenAndServe()
}

func New(cfg Config) *WebServer {
	srv := &http.Server{
		Addr:         cfg.Port,
		WriteTimeout: cfg.Timeout,
		ReadTimeout:  cfg.Timeout,
		IdleTimeout:  cfg.Timeout,
		ErrorLog:     log.New(io.Discard, "", 0),
	}

	sessionKey := uuid.New().String()

	return &WebServer{
		srv,
		sessionKey,
		cfg.Templates,
	}
}
