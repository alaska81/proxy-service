package entity

type Proxy struct {
	Proxy       string `db:"proxy"`
	CreatedAt   string `db:"created_at"`
	Error       string `db:"error"`
	CountErrors int    `db:"count_errors"`

	Port string
}
