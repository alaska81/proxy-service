package service

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"sync"
	"time"

	"proxy-service/internal/app/entity"
)

//go:generate minimock -i proxy-service/internal/app/service.Store -o ./store_mock.go -n StoreMock
type Store interface {
	List(context.Context) ([]entity.Proxy, error)
	Append(context.Context, []entity.Proxy) error
	Remove(context.Context, []entity.Proxy) error
}

//go:generate minimock -i proxy-service/internal/app/service.Logger -o ./log_mock.go -n LogMock
type Logger interface {
	Infof(format string, args ...interface{})
}

type Config struct {
	SizeChannel     int
	ValidateTimeout time.Duration
	DealTimeout     time.Duration
}

type proxyList struct {
	regular struct {
		mu   *sync.RWMutex
		list map[string]entity.Proxy
	}
	broken struct {
		mu   *sync.RWMutex
		list map[string]entity.Proxy
	}
	exchange struct {
		mu   *sync.RWMutex
		list map[string]chan entity.Proxy
	}
}

type ProxyService struct {
	proxyList
	cfg   Config
	store Store
	log   Logger
}

// var dialer *net.Dialer

var (
	ErrTimeout   = errors.New("cancellation by timeout")
	ErrEmptyList = errors.New("empty list proxies")
)

const urlTarget = "http://api.binance.com"

func (s *ProxyService) GetExchangeProxy(ctx context.Context, exchange string) (entity.Proxy, error) {
	s.exchange.mu.RLock()
	ch, ok := s.exchange.list[exchange]
	s.exchange.mu.RUnlock()

	if !ok {
		list := s.GetTotalList()
		if len(list) == 0 {
			return entity.Proxy{}, ErrEmptyList
		}

		ch = make(chan entity.Proxy, s.cfg.SizeChannel)
		s.exchange.mu.Lock()
		s.exchange.list[exchange] = ch
		s.exchange.mu.Unlock()

		for _, proxy := range list {
			ch <- proxy
		}

		s.log.Infof("Init exchange proxy channel %s, count: %d", exchange, len(list))
	}

	for {
		select {
		case <-ctx.Done():
			return entity.Proxy{}, ErrTimeout

		case proxy := <-ch:
			ch <- proxy

			if s.presentProxyInRegularList(proxy) {
				// exmo Proxy connection refused on :8000
				if exchange == "exmo" && proxy.Port == "8000" {
					continue
				}

				// huobi Request timeout on :24532
				// if exchange == "huobi" && proxy.Port == "24532" {
				// 	continue
				// }

				return proxy, nil
			}
		}
	}
}

func (s *ProxyService) InitList(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(ctx, time.Second*3)
	defer cancel()

	list, err := s.store.List(ctx)
	if err != nil {
		return err
	}

	s.regular.mu.Lock()
	for _, proxy := range list {
		s.regular.list[proxy.Proxy] = proxy
	}
	s.regular.mu.Unlock()

	s.log.Infof("Init (Regular: %v, Broken: %v)", len(s.regular.list), len(s.broken.list))

	return nil
}

func (s *ProxyService) AppendList(ctx context.Context, list []entity.Proxy) error {
	if err := s.store.Append(ctx, list); err != nil {
		return err
	}

	chRegular := make(chan entity.Proxy, 1)
	defer close(chRegular)

	chBroken := make(chan entity.Proxy, 1)
	defer close(chBroken)

	wg := sync.WaitGroup{}
	wg.Add(len(list))

	if s.cfg.ValidateTimeout > 0 {
		s.validateList(list, chRegular, chBroken)
	}

	go func() {
		for proxy := range chRegular {
			s.regular.mu.Lock()
			s.regular.list[proxy.Proxy] = proxy
			s.regular.mu.Unlock()

			for _, ch := range s.getExchangeChanList() {
				ch <- proxy
			}

			wg.Done()
		}
	}()

	go func() {
		for proxy := range chBroken {
			s.broken.mu.Lock()
			s.broken.list[proxy.Proxy] = proxy
			s.broken.mu.Unlock()
			wg.Done()
		}
	}()

	wg.Wait()

	s.log.Infof("Append (Regular: %v, Broken: %v)", len(s.regular.list), len(s.broken.list))

	// if s.terminal != nil {
	// 	ips := s.proxyToIPList(s.regular.list)
	// 	if err := s.terminal.SendIPs(ips); err != nil {
	// 		return fmt.Errorf("terminal: %s", err.Error())
	// 	}
	// 	s.log.Infof("Send IPs to terminal: %v", len(ips))
	// }

	return nil
}

func (s *ProxyService) RemoveList(ctx context.Context, list []entity.Proxy) error {
	if err := s.store.Remove(ctx, list); err != nil {
		return err
	}

	s.regular.mu.Lock()
	for _, delproxy := range list {
		for _, proxy := range s.regular.list {
			if strings.Contains(proxy.Proxy, delproxy.Proxy) {
				delete(s.regular.list, proxy.Proxy)
			}
		}
	}
	s.regular.mu.Unlock()

	s.broken.mu.Lock()
	for _, delproxy := range list {
		for _, proxy := range s.broken.list {
			if strings.Contains(proxy.Proxy, delproxy.Proxy) {
				delete(s.broken.list, proxy.Proxy)
			}
		}
	}
	s.broken.mu.Unlock()

	s.log.Infof("Remove (Regular: %v, Broken: %v)", len(s.regular.list), len(s.broken.list))

	return nil
}

func (s *ProxyService) getExchangeChanList() (list []chan entity.Proxy) {
	s.exchange.mu.RLock()
	for _, ch := range s.exchange.list {
		list = append(list, ch)
	}
	s.exchange.mu.RUnlock()

	return
}

func (s *ProxyService) GetTotalList() []entity.Proxy {
	list := make([]entity.Proxy, 0, len(s.regular.list)+len(s.broken.list))

	s.regular.mu.RLock()
	for _, proxy := range s.regular.list {
		list = append(list, proxy)
	}
	s.regular.mu.RUnlock()

	s.broken.mu.RLock()
	for _, proxy := range s.broken.list {
		list = append(list, proxy)
	}
	s.broken.mu.RUnlock()

	return list
}

func (s *ProxyService) GetRegularList(sorting bool) []entity.Proxy {
	list := make([]entity.Proxy, 0, len(s.regular.list))

	s.regular.mu.RLock()
	for _, proxy := range s.regular.list {
		list = append(list, proxy)
	}
	s.regular.mu.RUnlock()

	if sorting {
		sort.Slice(list, func(i, j int) bool {
			return list[i].CreatedAt < list[j].CreatedAt
		})
	}

	return list
}

func (s *proxyList) presentProxyInRegularList(proxy entity.Proxy) bool {
	s.regular.mu.RLock()
	defer s.regular.mu.RUnlock()

	if _, ok := s.regular.list[proxy.Proxy]; !ok {
		return false
	}

	return true
}

func (s *ProxyService) GetBrokenList(sorting bool) []entity.Proxy {
	list := make([]entity.Proxy, 0, len(s.broken.list))

	s.broken.mu.RLock()
	for _, proxy := range s.broken.list {
		list = append(list, proxy)
	}
	s.broken.mu.RUnlock()

	if sorting {
		sort.Slice(list, func(i, j int) bool {
			return list[i].CreatedAt < list[j].CreatedAt
		})
	}

	return list
}

// func (s *proxyList) presentProxyInBrokenList(proxy entity.Proxy) bool {
// 	s.broken.mu.RLock()
// 	defer s.broken.mu.RUnlock()

// 	if _, ok := s.broken.list[proxy.Proxy]; !ok {
// 		return false
// 	}

// 	return true
// }

func (s *ProxyService) RunValidate(ctx context.Context) {
	defer func() {
		s.log.Infof("Stop validate")
	}()

	if s.cfg.ValidateTimeout > 0 {
		ticker := time.NewTicker(s.cfg.ValidateTimeout)
		defer ticker.Stop()

		for {
			s.validate()

			select {
			case <-ctx.Done():
				return
			case <-ticker.C:
				// continue
			}
		}
	}
}

func (s *ProxyService) validate() {
	chRegular := make(chan entity.Proxy, 1)
	defer close(chRegular)

	chBroken := make(chan entity.Proxy, 1)
	defer close(chBroken)

	wg := sync.WaitGroup{}

	regularList := s.GetRegularList(false)
	brokenList := s.GetBrokenList(false)
	wg.Add(len(regularList) + len(brokenList))

	for _, proxy := range regularList {
		go func(proxy entity.Proxy) {
			if err := s.validateDial(proxy.Proxy); err != nil {
				s.log.Infof("broken proxy: %s: %v", proxy.Proxy, err)

				proxy.Error = err.Error()
				proxy.CountErrors++
				chBroken <- proxy
				return
			}

			chRegular <- proxy
		}(proxy)
	}

	for _, proxy := range brokenList {
		go func(proxy entity.Proxy) {
			if err := s.validateDial(proxy.Proxy); err != nil {
				// l.log.Infof("broken proxy: %s: %v", proxy.Proxy, err)

				proxy.Error = err.Error()
				proxy.CountErrors++
				chBroken <- proxy
				return
			}

			chRegular <- proxy
		}(proxy)
	}

	newRegularList := make(map[string]entity.Proxy)
	newBrokenList := make(map[string]entity.Proxy)

	go func() {
		for proxy := range chRegular {
			newRegularList[proxy.Proxy] = proxy
			wg.Done()
		}
	}()

	go func() {
		for proxy := range chBroken {
			newBrokenList[proxy.Proxy] = proxy
			wg.Done()
		}
	}()

	wg.Wait()

	s.regular.mu.Lock()
	s.regular.list = newRegularList
	s.regular.mu.Unlock()

	s.broken.mu.Lock()
	s.broken.list = newBrokenList
	s.broken.mu.Unlock()

	// fmt.Printf("[%s] Regular: %v, Broken: %v\n", time.Now().Format("2006-01-02 15:04:05"), len(s.regular.list), len(s.broken.list))
}

func (s *ProxyService) validateList(list []entity.Proxy, chRegular, chBroken chan entity.Proxy) {
	for _, proxy := range list {
		go func(proxy entity.Proxy) {
			if err := s.validateDial(proxy.Proxy); err != nil {
				s.log.Infof("broken proxy: %s: %v", proxy.Proxy, err)

				proxy.Error = err.Error()
				proxy.CountErrors++
				chBroken <- proxy
				return
			}

			proxy.Error = ""
			proxy.CountErrors = 0
			chRegular <- proxy
		}(proxy)
	}
}

func (s *ProxyService) validateDial(proxy string) error {
	ss := strings.Split(proxy, "@")
	if len(ss) < 2 {
		return errors.New("proxy not valid")
	}

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	// conn, err := dialer.DialContext(ctx, "tcp", ss[1])
	// if err != nil {
	// 	return err
	// }
	// defer conn.Close()

	proxyURL, err := url.Parse("socks5://" + proxy)
	if err != nil {
		return fmt.Errorf("url.Parse: %s: %v", proxy, err)
	}

	client := &http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyURL(proxyURL),
		},
	}

	request, _ := http.NewRequestWithContext(ctx, "GET", urlTarget, nil)
	response, err := client.Do(request)
	if err != nil {
		return err
	}

	defer response.Body.Close()

	if response.StatusCode != 200 {
		return fmt.Errorf("status code: %d", response.StatusCode)
	}

	return nil
}

func New(cfg Config, store Store, log Logger) *ProxyService {
	// dialer = &net.Dialer{
	// 	Timeout:   cfg.DealTimeout,
	// 	KeepAlive: cfg.DealTimeout,
	// }

	list := proxyList{
		regular: struct {
			mu   *sync.RWMutex
			list map[string]entity.Proxy
		}{
			mu:   &sync.RWMutex{},
			list: make(map[string]entity.Proxy),
		},
		broken: struct {
			mu   *sync.RWMutex
			list map[string]entity.Proxy
		}{
			mu:   &sync.RWMutex{},
			list: make(map[string]entity.Proxy),
		},
		exchange: struct {
			mu   *sync.RWMutex
			list map[string]chan entity.Proxy
		}{
			mu:   &sync.RWMutex{},
			list: make(map[string]chan entity.Proxy),
		},
	}

	return &ProxyService{
		list,
		cfg,
		store,
		log,
	}
}
