package service_test

import (
	"context"
	"testing"
	"time"

	"proxy-service/internal/app/entity"
	"proxy-service/internal/app/service"

	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
)

var proxyConfig service.Config = service.Config{
	SizeChannel: 5,
	DealTimeout: 10,
}

func TestGetExchangeProxyEmptyList(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	storeMock := service.NewStoreMock(mc)
	logMock := service.NewLogMock(mc)

	proxyService := service.New(proxyConfig, storeMock, logMock)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err := proxyService.GetExchangeProxy(ctx, "")
	assert.ErrorIs(t, err, service.ErrEmptyList)
}

func TestGetExchangeProxy(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	storeMock := service.NewStoreMock(mc)
	list := []entity.Proxy{
		{Proxy: "proxy1"},
		{Proxy: "proxy2"},
		{Proxy: "proxy3"},
	}
	storeMock.ListMock.Return(list, nil)

	logMock := service.NewLogMock(mc)

	proxyService := service.New(proxyConfig, storeMock, logMock)
	proxyService.InitList(context.Background())

	exchange := "exchange_test"
	var expectedList []entity.Proxy

	for i := 0; i < len(list); i++ {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		proxy, err := proxyService.GetExchangeProxy(ctx, exchange)
		assert.Nil(t, err)
		expectedList = append(expectedList, proxy)
	}

	assert.ElementsMatch(t, list, expectedList)
}
