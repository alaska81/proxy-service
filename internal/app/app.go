package app

import (
	"context"
	"errors"
	"log"
	"net/http"
	"time"

	"proxy-service/internal/adapters/api"
	"proxy-service/internal/adapters/store"
	"proxy-service/internal/adapters/terminal"
	"proxy-service/internal/app/service"
	"proxy-service/internal/pkg/config"
	"proxy-service/internal/pkg/db/sqlite"
	"proxy-service/internal/pkg/logger"
	"proxy-service/internal/pkg/webserver"
)

type App struct {
	proxy    *service.ProxyService
	web      *webserver.WebServer
	store    *store.StoreSQLite
	log      *logger.Logger
	terminal *terminal.Terminal
	password string
}

func (a *App) RunWebServer(ctx context.Context) {
	var err error
	a.web.Handler, err = api.NewRouter(a.proxy, a.web.SessionKey, a.web.Templates, a.password, a.log, a.terminal)
	if err != nil {
		log.Panic(err)
	}

	go func() {
		if err := a.web.Start(); err != nil {
			if !errors.Is(err, http.ErrServerClosed) {
				log.Panic(err)
			}
		}
	}()

	a.log.Infof("Start on %s", a.web.Addr)
}

func (a *App) StopWebServer(ctx context.Context) {
	a.log.Infof("Stopped")

	if a.web != nil {
		ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
		defer cancel()

		a.web.Shutdown(ctx)
	}
}

func (a *App) RunProxyService(ctx context.Context) {
	a.proxy.InitList(ctx)
	a.proxy.RunValidate(ctx)
}

func New(cfg *config.Config) (*App, error) {
	logConfig := logger.Config{
		Level: cfg.Logger.Level,
		Path:  cfg.Logger.Path,
		File:  cfg.Logger.File,
	}
	log, err := logger.New(logConfig)
	if err != nil {
		return nil, err
	}

	dbConfig := sqlite.Config{
		Path:         cfg.Sqlite.Path,
		Filename:     cfg.Sqlite.File,
		MaxOpenConns: cfg.Sqlite.MaxOpenConns,
		MaxIdleConns: cfg.Sqlite.MaxIdleConns,
	}
	db, err := sqlite.NewClient(dbConfig)
	if err != nil {
		return nil, err
	}

	store := store.NewStoreSQLite(db, log)

	webConfig := webserver.Config{
		Port:      cfg.WebServer.Port,
		Timeout:   time.Duration(cfg.WebServer.Timeout) * time.Second,
		Templates: cfg.WebServer.Templates,
	}
	web := webserver.New(webConfig)

	terminal := terminal.NewClient(cfg.Terminal.URL)

	proxyConfig := service.Config{
		SizeChannel:     cfg.SizeChannel,
		ValidateTimeout: cfg.Service.ValidateTimeout,
		DealTimeout:     cfg.Service.DealTimeout,
	}
	proxyService := service.New(proxyConfig, store, log)

	return &App{
		proxy:    proxyService,
		web:      web,
		store:    store,
		log:      log,
		terminal: terminal,
		password: cfg.Password,
	}, nil
}
