package main

import (
	"context"
	"log"
	"os/signal"
	"syscall"

	"proxy-service/internal/app"
	"proxy-service/internal/pkg/config"
)

func main() {
	cfg, err := config.GetFromFile(configFile)
	if err != nil {
		log.Panic(err)
	}

	app, err := app.New(cfg)
	if err != nil {
		log.Panic(err)
	}

	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()

	go app.RunProxyService(ctx)
	app.RunWebServer(ctx)

	<-ctx.Done()

	app.StopWebServer(ctx)
}
