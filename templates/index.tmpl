{{define "index"}}

<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Proxy Service</title>
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

    <style>
        body {
            background: #eee;
        }

        form {
            width: calc(100% - 20px);
            max-width: 300px;
            background-color: #fff;
            padding: 10px;
            box-shadow: 0px 3px 10px #d4cdcd;
            margin: 50px auto;
            border-radius: 10px;
        }
        textarea {
            width: calc(100% - 45px);
            padding: 20px;
            height: 200px;
            border: 1px solid #ececec;
            border-radius: 10px;
            outline: none;
        }

        form.delete textarea {
            height: 14px;
        }

        button {
            width: 100%;
            padding: 10px;
            border: 0;
            background: #333;
            color: #fff;
            border-radius: 10px;
            outline: none;
            cursor: pointer;
        }

        p.proxy {
            width: calc(100% - 20px);
            max-width: 300px;
            background-color: #fff;
            padding: 10px;
            box-shadow: 0px 3px 10px #d4cdcd;
            margin: 10px auto;
            text-align: center;
            border-radius: 10px;
        }
        .request {
            background-color: #fff;
            padding: 10px;
            box-shadow: 0px 3px 10px #d4cdcd;
            margin: 10px auto;
            border-radius: 10px;
        }
        .proxy_list {
            height: 450px;
            overflow: auto;
            margin: 0px auto;
            flex-grow: 1;
        }
        h2 {
            width: 100%;
            text-align: center;
            margin: 0;
        }
        h3 {
            width: 100%;
            text-align: center;
            font-size: 24px;
            margin: 0;
        }
        #loading {
            position: fixed;
            top: calc(50% - 14px);
            left: calc(50% - 14px);
            z-index: 999999;
            display: none;
        }
        i.icon-refresh {
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="mdl-grid">
        <h2>Proxy Service</h2>
        <p class="request">GET /proxy/get/{exchange} - получить прокси для биржи {exchange}</p>
    </div>
    <div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active" id="loading"></div>
    <div class="mdl-grid">
        <div class="mdl-cell mdl-cell--6-col">
            <h2>Setting</h2>
            <hr>
            <div class="request" id="error" {{if not .Error}}style="display:none;"{{end}} onclick="this.style.display = 'none'">{{.Error}}</div>
            <form method="post" class="add">
                <input type="hidden" name="action" value="add"/>
                <textarea name="list">
{{- range $key, $proxy := .List -}}
{{ $proxy.Proxy }}
{{ end -}}
                </textarea>
                <button type="submit" onclick="document.getElementById('loading').style.display = 'block';">Add proxy</button>
            </form>
            <form method="post" class="delete">
                <input type="hidden" name="action" value="del"/>
                <textarea name="list" id="delete_list"></textarea>
                <button type="submit" onclick="document.getElementById('loading').style.display = 'block';">Search and delete</button>
            </form>
        </div>
        <div class="mdl-cell mdl-cell--6-col">
            <h2>
                <i class="icon-refresh" onclick="window.location.replace('/')"></i> Proxy {{len .RegularList}} / {{ len .BrokenList }} 
                {{if .Terminal}}
                    <button style="width:auto;" onclick="sentToTerminal()">Send to Terminal</button>
                {{end}}
            </h2>
            <hr>
            <div style="display: flex;">
                <div class="mdl-cell mdl-cell--6-col">
                    <h3>Regular</h3>
                    <div class="proxy_list">
                        {{ range $key, $proxy := .RegularList }}
                            <p class="proxy">
                                {{ $proxy.Proxy }}<br>
                                {{ $proxy.CreatedAt }}
                                <button onclick="append_del({{ $proxy.Proxy }})">Del</button>
                            </p>
                        {{ end }}
                    </div>
                </div>
                <div class="mdl-cell mdl-cell--6-col">
                    <h3>Broken</h3>
                    <div class="proxy_list">
                        {{ range $key, $proxy := .BrokenList }}
                            <p class="proxy">
                                {{ $proxy.Proxy }}<br>
                                {{ $proxy.CreatedAt }} [Errors: {{ $proxy.CountErrors }}]
                                <button onclick="append_del({{ $proxy.Proxy }})">Del</button>
                            </p>
                        {{ end }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function append_del(proxy) {
            let $textarea = document.getElementById('delete_list');
            let rows = $textarea.value.split("\n");
            if (rows.indexOf(proxy) != -1) {
                alert("Proxy was present in list!");
                return;
            }

            document.getElementById('delete_list').append(proxy + "\n");
        }
        async function sentToTerminal() {
            let url = '/terminal/ip';
            let response = await fetch(url);
            let data = await response.json();
            if (!data.success) {
                alert(data.error);
                return;
            }
            alert('success');
        }
    </script>
</body>

{{end}}